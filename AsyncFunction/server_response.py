import asyncio
import time

# Response delay imitation.
def get_response(count):
    print("Send request...")
    time.sleep(3)
    print(f"<h1>Response Content {count}</h1>")


# Traditional way to get multiple response without multi-threading.
def requests():
    get_response(1)
    get_response(2)
    get_response(3)


# Response delay imitation.
async def get(count):
    print("Send request...")
    await asyncio.sleep(3)
    print(f"<h1>Response Content {count}</h1>")


# Use async function to get multiple response.
async def requests_async():
    await asyncio.gather(get(1), get(2), get(3))


print("How sync function suppose to work:")
requests()

print("How Async function suppose to work:")
asyncio.run(requests_async())
