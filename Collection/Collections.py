# Collection counter for creating a list from string.
from collections import Counter

sample0 = "aaaaabbbc"
my_counter = Counter(sample0)
print(f"Sample string: {sample0}")
print(f"Counter: {my_counter}")
for key, value in my_counter.items():
    print(f"{key}:{value}")
print(list(my_counter.elements()))

# Name tuple for creating a vector.
from collections import namedtuple

vector = namedtuple("vector", "x,y")
vt = vector(1, -4)
print(vt)
print(f"Vector contains a coordinate x={vt.x} y={vt.y}")

# Ordered dictionary.
# Ordered dictionary is used to create a dictionary which could maintain its original order.
# After Python 3.7 you don't have to worry about it, use '{}' instead.
from collections import OrderedDict

my_dict = OrderedDict()
my_dict["Galahad"] = 1000
my_dict["Peter"] = 135
my_dict["Artoria"] = 708
print(my_dict)

# Default dictionary, which is used to assign the default value.
# It helps to avoid KeyError when you give a key which not exist.
from collections import defaultdict

another_dict = defaultdict(float)
another_dict["Galahad"] = 1000
another_dict["Peter"] = 135
another_dict["Artoria"] = 708
print(another_dict["Galahad"])
print(another_dict["Ishtar"])

# Deque is just a queue.
from collections import deque

my_queue = deque()
print("Append two people.")
my_queue.append("Galahad")
my_queue.append("Peter")
print(f"Result: {my_queue}")

print("Append to left.")
my_queue.appendleft("Artoria")
print(f"Result: {my_queue}")

print("Pop one from the queue.")
my_queue.pop()
print(f"Result: {my_queue}")

print("Pop one from the left.")
my_queue.popleft()
print(f"Result: {my_queue}")
