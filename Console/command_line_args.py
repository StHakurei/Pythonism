import argparse

# Create an argument parser and prepare your arguments with neccesary information.
parser = argparse.ArgumentParser(description="Python console (v1.0)")
parser.add_argument("--database", default="127.0.0.1", help="Target database address.")
parser.add_argument("--user", default=None, help="Database user id.")
parser.add_argument("--passwd", default=None, help="Database sign on password.")
parser.add_argument("-V", "--verbose", action="store_true", help="Output detail.")

# Read arguments from command line.
args = parser.parse_args()
db_address = args.database
uuid = args.user
passwd = args.passwd

# Check if user gives arguments in command line.
if db_address is None or uuid is None or passwd is None:
    print("Hey, we are just starting without database!")
else:
    print(db_address)
    print(uuid)
    print(passwd)
    if args.verbose:
        print("Got you, trying to connect to database with details!")
    else:
        print("Got you, trying to connect to database!")

# We done here.
