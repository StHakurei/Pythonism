# Define user defined exceptions.
class Error(Exception):
    """Base class for other exceptions."""
    pass

class ZeroNumberError(Error):
    """Exception raise for number 0 detection."""
    pass

class NegativeNumberError(Error):
    """Exception raise for negative number detection."""
    pass


# Customizing exception classes.
class OutOfRangeError(Exception):
    """Exception raise for errors input numbers are out of ranges.

    Attributes:
        given_number -- input number.
        message -- explanation of the error.
    """

    def __init__(self, given_number, message="Given number is not in range (10, 100)"):
        self.given_number = given_number
        self.message = message
        super().__init__(self.message)


# Use user-defined exception.
user_input = int(input("Enter a number:"))
if user_input == 0:
    raise ZeroNumberError

if user_input < 0:
    raise NegativeNumberError

# Use customizing exception.
user_input = int(input("Enter a number:"))
if not user_input in range(10, 101):
    raise OutOfRangeError(user_input)


"""Expected output:

** ZeroNumberError **
Enter a number:0
Traceback (most recent call last):
  File "custom_exception.py", line 33, in <module>
    raise ZeroNumberError
__main__.ZeroNumberError

** NegativeNumberError **
Enter a number:-1
Traceback (most recent call last):
  File "custom_exception.py", line 36, in <module>
    raise NegativeNumberError
__main__.NegativeNumberError

** OutOfRangeError **
Enter a number:1
Enter a number:1
Traceback (most recent call last):
  File "custom_exception.py", line 41, in <module>
    raise OutOfRangeError(user_input)
__main__.OutOfRangeError: Given number is not in range (10, 100)
"""