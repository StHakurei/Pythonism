# Decorator extends the behavior of the functions.
def extends_behavior(func):
    def wrapper():
        print("Decorator start:")
        func()
        print("Decorator out.")

    return wrapper


@extends_behavior
def showAuthor():
    print("Repo author = Langston.Hakurei")


def extends_behavior_1(func):
    def wrapper(*args, **kwargs):
        print("Decorator start:")
        print("Received args:")
        for a in args:
            print(a)
        result = func(*args, **kwargs)
        print("Decorator out.")
        return result

    return wrapper


@extends_behavior_1
def calVolumn(radius, height):
    return radius * 2 * 3.14 * height


# See the output.
showAuthor()
print(f"Result = {calVolumn(10, 20)} sequares.")
