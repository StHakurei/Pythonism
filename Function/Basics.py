# Pass a varying number of positional arguments.
def display_args(*args):
    for arg in args:
        print(arg)


# 'args' is just a name
def auto_sum(*integers):
    result = 0
    for integer in integers:
        result = result + integer
    return result


# **kwags work as *args, but instead of accepting positional arguments, it
# accepts keyword arguments.
def contacts(**kwargs):
    for key, value in kwargs.items():
        print(f"{key} = {value}")


display_args(1, 2, 3)
print(f"Result = {auto_sum(1, 2, 3)}")
contacts(a=1, b=2, c=3)
