import time


def without_callback(max):
    count = 0
    while count < max:
        time.sleep(1)
        count = count + 1
    print("Counting was done.")


def with_callback(max, func):
    count = 0
    while count < max:
        time.sleep(1)
        count = count + 1
        callback(count)
    print("Counting was done.")


def callback(message):
    print(f"Progress {message}")


without_callback(5)
with_callback(5, callback)
