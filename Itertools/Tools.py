# Product.
from itertools import product

a = [10, 20]
b = [15, 35]
my_prod = product(a, b)
print("Product will give you every possible tuples of two given lists.")
print(f"a = {a}")
print(f"b = {b}")
print(f"product(a, b) = {list(my_prod)}")

# Permutation.
from itertools import permutations

a = [1, 2, 3]
my_permu = permutations(a)
print("Permutation will give you every possible order of the list.")
print(f"a = {a}")
print(f"permutations(a) = {list(my_permu)}")

# Combination.
from itertools import combinations

a = [1, 2, 3, 4]
my_combin = combinations(a, 2)
print("Combination will give you every possible combination of the element.")
print(f"a = {a}")
print(f"combinations(a, 2) = {list(my_combin)}")

# Accumulation.
from itertools import accumulate

a = [1, 2, 3, 4]
my_accu = accumulate(a)
print("Accumualte give give you a new list based on the input list.")
print(f"a = {a}")
print(f"combinations(a, 2) = {list(my_accu)}")
