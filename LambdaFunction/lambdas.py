# lambda args: expression.
circle = lambda radius: radius * 2 * 3.14
print(f"Circle area = {circle(10)}")

volume = lambda radius, height: radius * 2 * 3.14 * height
print(f"Cylindar volume = {volume(10, 10)}")
