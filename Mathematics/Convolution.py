import numpy as np

from typing import Final
from numpy import ndarray
from scipy.signal import convolve2d

from utils import inspect_array


def exec_convolve2d(data: ndarray, kernel: ndarray, padding: bool = False) -> ndarray:
    if padding:
        data = np.pad(data, pad_width=1, mode="constant", constant_values=0)

    KERNEL_H, KERNEL_W = kernel.shape
    DATA_H, DATA_W = data.shape

    output = np.zeros(
        shape=(DATA_H - KERNEL_H + 1, DATA_W - KERNEL_W + 1), dtype=np.uint8
    )

    OUTPUT_H, OUTPUT_W = output.shape

    for index_h in range(OUTPUT_H):
        for index_w in range(OUTPUT_W):
            select_area = data[
                index_h : index_h + KERNEL_H, index_w : index_w + KERNEL_W
            ]
            output[index_h, index_w] = np.sum(select_area & kernel)

    return output


KERNEL_0: Final = np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]], dtype=np.uint8)

INPUT_DATA: Final = np.random.randint(2, size=(4, 4), dtype=np.uint8)


inspect_array(KERNEL_0, "KERNEL_0", True)
inspect_array(INPUT_DATA, "INPUT_DATA", True)

convolve2d_manual_result = exec_convolve2d(INPUT_DATA, KERNEL_0, True)
inspect_array(convolve2d_manual_result, "convolve2d_manual_result", True)

CONVO2D_RESULT_0 = convolve2d(INPUT_DATA, KERNEL_0, mode="same")
inspect_array(CONVO2D_RESULT_0, "CONVO2D_RESULT_0", True)
