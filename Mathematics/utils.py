from numpy import ndarray
from typing import Optional


def inspect_array(input: ndarray, name: Optional[str], output: bool = False) -> None:
    print(
        "Inspect NumPy Array:",
        f'"{name}"' if name else "unknwon",
        f"-> dimension: {input.ndim}, shape: {input.shape}, elements: {input.size}, type: {input.dtype.name}",
    )
    if output:
        print(input)
