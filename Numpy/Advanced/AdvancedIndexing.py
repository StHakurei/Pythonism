import numpy as np

# Indexing with Arrays of indices.
array_0 = np.arange(12) ** 2  # Create an array with the first 12 square integers.
array_1 = np.array([0, 2, 4, 6, 8])  # An array of indices.
array_2 = np.array([[0, 2], [4, 6]])  # A bidimensional array of indices.
print(f"Array of numbers: {array_0}")
print(f"Array of indices: {array_1}")
print(f"Get the elements of array_0 at the positions array_1: {array_0[array_1]}")
print(f"Get the elements of array_0 at the positions array_2:\n{array_0[array_2]}")

# Do same thing in a multidimensional array.
palette = np.array([[0, 0, 0], [255, 0, 0], [0, 255, 0], [0, 0, 255], [255, 255, 255]])
# [R, G, B]
# [0, 0, 0] = Black, [255, 0, 0] = Red, [0, 255, 0] = Green, [0, 0, 255] = Green, [255, 255, 255] = White.
pexels = np.array([[1, 2, 2, 0], [0, 4, 3, 1]])
print(f"Display pexels of an image:\n{palette[pexels]}")

# We can also give indexes for more than one dimension.
array_3 = np.arange(12).reshape(3, 4)
print(f"Original Array:\n{array_3}")

indexes_0 = np.array([[0, 1], [2, 0]])  # Index of array for the first dimension.
indexes_1 = np.array([[2, 1], [1, 3]])  # Index of array for the second dimension.

print(f"indexes_0:\n{indexes_0}")
print(f"indexes_1:\n{indexes_1}")

# indexes_0 and indexes_1 must have same shape.
print(f"Output array_3[indexes_0, indexes_1]:\n{array_3[indexes_0, indexes_1]}")

# Another approache via tuple.
index_tuple = (indexes_0, indexes_1)
print(f"Output array_3[index_tuple]:\n{array_3[index_tuple]}")

# Search maximum value of time-dependent series by indexing.
data = np.sin(np.arange(20)).reshape(5, 4)
print(f"Sample array:\n{data}")

index_of_maximum_value_of_axis_x = data.argmax(axis=1)
index_of_maximum_value_of_axis_y = data.argmax(axis=0)
print(
    f"The index of the maximum values on horizental axis: {index_of_maximum_value_of_axis_x}"
)
print(
    f"The index of the maximum values on vertical axis: {index_of_maximum_value_of_axis_y}"
)

max_value_on_axis_x = data[range(data.shape[0]), index_of_maximum_value_of_axis_x]
max_value_on_axis_y = data[index_of_maximum_value_of_axis_y, range(data.shape[1])]
print(f"The maximum values on horizental axis: {max_value_on_axis_x}")
print(f"The maximum values on vertical axis: {max_value_on_axis_y}")

# Batch modify value of an array by indexing.
array_4 = np.arange(10)
print(f"Original array: {array_4}")

array_4[[2, 3, 4]] = 0
print(f"After 'array_4[[2,3,4]] = 0': {array_4}")

array_4[[2, 3, 4]] = [2, 3, 4]
print(f"After 'array_4[[2, 3, 4]] = [2, 3, 4]': {array_4}")

array_4[[2, 3, 4]] += 100
print(f"After 'array_4[[2, 3, 4]] += 1': {array_4}")

array_4[[2, 3, 4]] -= 100
print(f"After 'array_4[[2, 3, 4]] -= 1': {array_4}")
