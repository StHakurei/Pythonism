import numpy as np

# To change the dimensions of an array, you can omit one of the sizes
# which will then be deduced automatically.
a = np.arange(30)
b = a.reshape((2, -1, 3))
print(f"Array A: Shape({a.shape}):\n{a}")
print(f"Array B: Shape({b.shape}):\n{b}")
