import numpy as np
import matplotlib.pyplot as plt

# Generate an array contains 10000 random numbers.
rg = np.random.default_rng(1)
mu, sigma = 2, 0.5
v = rg.normal(mu, sigma, 10000)
print(f"Array V:\nShape {v.shape}\nSize {v.size}")


plt.hist(v, bins=50, density=True)
(n, bins) = np.histogram(v, bins=50, density=True)
plt.plot(0.5 * (bins[1:] + bins[:-1]), n)
plt.show()
