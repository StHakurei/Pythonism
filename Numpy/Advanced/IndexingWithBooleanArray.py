import numpy as np

data_array = np.arange(12).reshape(3, 4)
boolean_array = data_array > 6
selected_array = data_array[boolean_array]

# The boolean_array is bool array with the shape of data_array.
print(f"Data Array:\n{data_array}")
print(f"Boolean Array (where data_array > 6):\n{boolean_array}")
print(f"Output selected elements by boolean_array:\n{selected_array}")

# Modify selected elements by boolean_array.
data_array[boolean_array] = 100
print(f"Modified data_array:\n{data_array}")

# Select multi-dim elements by boolean_array.
data_array = np.arange(12).reshape(3, 4)
boolean_array_1 = np.array([False, True, True])
boolean_array_2 = np.array([False, True, True, False])

print(f"Data Array:\n{data_array}")
print(f"Select rows:\n{data_array[boolean_array_1,:]}")
print(f"Select columns:\n{data_array[:,boolean_array_2]}")
print(
    f"Select elements are True in both bool arrays:\n{data_array[boolean_array_1,boolean_array_2]}"
)
