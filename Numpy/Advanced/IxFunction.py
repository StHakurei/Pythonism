import numpy as np

# xi() function could help you to combine different vectors.
a = np.array([1, 2, 3])
b = np.array([4, 5, 6, 7])
c = np.array([8, 9])

ax, bx, cx = np.ix_(a, c, b)
print(f"ax:\n{ax}")
print(f"bx:\n{bx}")
print(f"cx:\n{cx}")

# Compare the changes of their shapes.
print(f"{a.shape} => {ax.shape}")
print(f"{b.shape} => {bx.shape}")
print(f"{c.shape} => {cx.shape}")

result_0 = ax + bx
result_1 = bx * cx
result_3 = ax + bx * cx

# Output result.
print(f"ax + bx:\n{result_0}")
print(f"bx * cx:\n{result_1}")
print(f"ax + bx * cx:\n{result_3}")

print(a.identity)
