# Use boolean indexing to generate an image of the Mandelbrot set.
#
# About Mandelbrot set: https://en.wikipedia.org/wiki/Mandelbrot_set
#
import numpy as np
import matplotlib.pyplot as plt


def mandelbrot(height, width, maxit=20, r=2):
    x = np.linspace(-2.5, 1.5, 4 * height + 1)
    y = np.linspace(-1.5, 1.5, 3 * width + 1)
    A, B = np.meshgrid(x, y)
    C = A + B * 1j
    z = np.zeros_like(C)
    divtime = maxit + np.zeros(z.shape, dtype=int)

    for i in range(maxit):
        z = z ** 2 + C
        diverge = abs(z) > r  # who is diverging
        div_now = diverge & (divtime == maxit)  # who is diverging now
        divtime[div_now] = i  # note when
        z[diverge] = r  # avoid diverging too much

    return divtime


plt.imshow(mandelbrot(400, 400))
plt.show()
