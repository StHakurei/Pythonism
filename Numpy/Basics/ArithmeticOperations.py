import numpy as np

first_array = np.array([10, 20, 30, 40])
second_array = np.arange(4)

# Arithmetic operations on arrays apply elementwise, they always return a new array as result.
third_array = first_array - second_array
fourth_array = second_array ** 2
boolean_array = first_array > 25

print(f"First array: {first_array}")
print(f"Second array: {second_array}")
print(f"First array - Second array = {third_array}")
print(f"Second array to the power of 2 = {fourth_array}")
print(f"Is the element of First array > 25? {boolean_array}")

array_0 = np.array([0, 1, 2])
array_1 = np.array([3, 4, 5])
print(f"What is the result of adding array_0 with array_1? {np.add(array_0, array_1)}")

base_array = np.arange(10)
print(f"Output base array: {base_array}")
print(f"Each element to the power of 3: {base_array ** 3}")
print(f"Get the last element: {base_array[9]}")
print(f"Get the first three elements: {base_array[0:3]}")
base_array[0:6:2] = 255
print(f"From the first to postion 6, set every second element to 255: {base_array}")


array_a = np.array([[0, 1], [1, 3]])
array_b = np.array([[2, 3], [0, 4]])
print(f"Array A is:\n{array_a}")
print(f"Array B is:\n{array_b}")
print(f"Elementwise product:\n{array_a * array_b}")
print(f"matrix product:\n{array_a @ array_b}")

# Some operators, such as += and *=, act in place to modify an existing array rather than create a new one.
array_a *= 3
print(f"In place modification:\n{array_a}")

# Create instance of default random number generator.
rng = np.random.default_rng(1)
random_array = rng.random((2, 3))
print(f"Create an random array:\n{random_array}")
print(f"The sum of random array: {random_array.sum()}")
print(f"The max of random array: {random_array.max()}")
print(f"The min of random array: {random_array.min()}")

random_matrix = rng.random(12).reshape(3, 4)
print(f"Random matrix:\n{random_matrix}")
print(f"The sum of each column: {random_matrix.sum(axis=0)}")
print(f"The sum of each row: {random_matrix.sum(axis=1)}")
print(f"The max of each column: {random_matrix.max(axis=0)}")
