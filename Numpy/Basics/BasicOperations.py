import numpy as np
from numpy import ndarray

# One-dimensional arrays can be indexed, sliced and iteratred over.
one_dimensional_array = np.arange(10) * 3
print("=== One-dimensional Array ===")
print(f"array elements -> {one_dimensional_array}")
print(f"array index [2] -> {one_dimensional_array[2]}")
print(f"array slice [2:4] -> {one_dimensional_array[2:4]}")
print(f"array reverse [::-1] -> {one_dimensional_array[::-1]}")

# Multi-dimensional arrays can have one index per axis, these indices are given in a tuple separated by commas.
def getArrays(x: int, y: int):
    return 10 * x + y


multi_dimensional_array = np.fromfunction(getArrays, (4, 5), dtype=int)
print("\n=== Multi-dimensional Array ===")
print(f"array elements:\n{multi_dimensional_array}")
print(f"array index [0,0] -> {multi_dimensional_array[0,0]}")
print(f"array index [3,4] -> {multi_dimensional_array[3,4]}")
print(f"array slice [0,0:5] -> {multi_dimensional_array[0,0:5]}")
print(f"array slice [0,:5] -> {multi_dimensional_array[0,:5]}")
print(f"array slice [2,1:4] -> {multi_dimensional_array[2,1:4]}")
# When fewer indices are provided than the number of axes, the missing indices are considered complete slices.
print(f"array slice [-1] -> {multi_dimensional_array[-1]}")
# You can use "dots" to represent remaining axies and elements.
print(f"array [0, ...] equals [0,0:]-> {multi_dimensional_array[0, ...]}")


# Iterating over multidimensional array.
for element in multi_dimensional_array:
    print(element)  # Per line output.

for element in multi_dimensional_array.flat:
    print(element)  # Per element output.
