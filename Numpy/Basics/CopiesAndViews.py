import numpy as np

rng = np.random.default_rng(1)
original_array = np.array(10 * rng.random((3, 4)), dtype=np.int16)

# No new array will be created.
new_array = original_array
print(f"Is new_array literally original_array itself?: {new_array is original_array}")

# Shallow copy.
print("=== Shallow copy sample ===")
copy_array = original_array.view()
print(f"Is copy_array literally original_array itself?: {copy_array is original_array}")
print(
    f"Is copy_array's data based on original_array?: {copy_array.base is original_array}"
)

# Reshape doesn't effect based array.
copy_array = copy_array.reshape((2, 6))
print(
    f"After reshape.\ncopy_array.shape = {copy_array.shape}\noriginal_array.shape = {original_array.shape}"
)
copy_array = copy_array.reshape((3, 4))

# Change data will effect each other.
copy_array[0, 2] = 255
print("copy_array[0, 2] = 255")
print(original_array)
print(copy_array)

original_array[0, 2] = 64
print("original_array[0, 2] = 64")
print(original_array)
print(copy_array)

# Deep copy.
# The "copy" method will completely copy all elements of the array and create a new object.
print("=== Deep copy sample ===")
clone_array = original_array.copy()
print(
    f"Is clone_array literally original_array itself?: {clone_array is original_array}"
)
print(
    f"Is clone_array's data based on original_array?: {clone_array.base is original_array}"
)

# The "copy" method could be called after slicing.
partially_array = original_array[0:3, 0:2].copy()
print(f"original_array:\n{original_array}")
print(f"partially_array:\n{partially_array}")
del original_array  # The memory of the original_array can be released.
