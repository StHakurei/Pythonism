# NumPy's main object is the homogeneous multidimensional array.
# After this sample code, you will understand the difference between one-, two- and n-dimensional arrays in NumPy.
import numpy as np
from numpy import ndarray

# NumPy's array is called "ndarray".
# In NumPy dimensions are called "axes".
# For example, [1, 2, 3] has one axis. [[1, 1], [2, 2]] has two axes.

# NumPy's array object has some important attributes.
def displayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")
    

one_dimensional_array = np.array([0, 1, 2, 3])
print(f"Create an one dimension array: \n {one_dimensional_array}")
print("*This ndarray has 1 axis and its length is 4.")
displayAttributes(one_dimensional_array)


multi_dimensional_array = np.arange(15).reshape(3, 5)
print(f"\nCreate a multidimensional array: \n {multi_dimensional_array}")
print("*This ndarray has 3 axes and each axis' length is 5.")
displayAttributes(multi_dimensional_array)


# Creaet an empty array with fixed size.
# Often, you need to create an array but its elements are unknwon, NumPy offers several functions to
# create arrays with initial placeholder content.
empty_array_0 = np.zeros((3, 5))
print(
    "\nCreate an array with placeholder number '0' and by default the data type is"
    f" 'float64': \n{empty_array_0}"
)
displayAttributes(empty_array_0)

empty_array_1 = np.ones((2, 3, 4), dtype=np.int16)
print(
    "\nCreate an array with placeholder number '1' and data type is 'int16':"
    f" \n{empty_array_1}"
)
displayAttributes(empty_array_1)

empty_array_true = np.empty((2, 3))
print(f"\nCreate an array without specify a placeholder: \n{empty_array_true}")
displayAttributes(empty_array_true)

# There's more flxible way to create arrays in NumPy.
print(
    "\nCreate an array from range 10 to 30 with specified interval 5:"
    f" \n{np.arange(10, 30, 5)}"
)
print(
    "\nCreate an array from range 0 to 3 with specified interval 0.3:"
    f" \n{np.arange(0, 3, 0.3)}"
)

print(
    "\nCreate an array from range 10 to 20 with demand quantity (5) of elements:"
    f" \n{np.linspace(10, 20, 5)}"
)
