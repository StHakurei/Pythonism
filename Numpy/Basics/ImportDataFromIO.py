import numpy as np
from numpy import ndarray
from io import StringIO


def displayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")


sample_data = """
Name,Model,SN,Manufacturer\n
Galaxy Note 9,SM-N960F,SDQWE123SEQ,Samsung\n
iPhone 13,MLE23CH/A,QWENUI641W,Apple\n
Name,Model,SN,Manufacturer
"""
data_type = np.dtype("U20, U20, U20, U10")

# Import array with specified data type.
imported_array_0 = np.genfromtxt(StringIO(sample_data), delimiter=",", dtype=data_type)
print(f"imported_array_0:\n{imported_array_0}")
displayAttributes(imported_array_0)

# Skip header.
imported_array_1 = np.genfromtxt(
    StringIO(sample_data), delimiter=",", dtype=data_type, skip_header=2
)
print(f"Skip header:\n{imported_array_1}")

# Skip footer.
imported_array_2 = np.genfromtxt(
    StringIO(sample_data), delimiter=",", dtype=data_type, skip_footer=1
)
print(f"Skip footer:\n{imported_array_2}")

# Autostrip.
sample_string = """
1, 2   , 3,   4\n
  5,6, 7 ,8
"""
without_strip = np.genfromtxt(StringIO(sample_string), delimiter=",")
print(f"Without autostrip:\n{without_strip}")

with_strip = np.genfromtxt(StringIO(sample_string), delimiter=",", autostrip=True)
print(f"With autostrip:\n{with_strip}")

# Ignore comments.
# By default, genfromtxt assumes comments='#'.
commented_string = """
# Comment 1.
# Comment 2.
1,2,3,4\n
# below numbers are greater than 100.
200,300,400,500\n
201,301,401,501
"""
comment_array = np.genfromtxt(StringIO(commented_string), delimiter=",", comments="#")
print(f"comment_array:\n{comment_array}")
