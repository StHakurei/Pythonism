import numpy as np

# The shape of an array can be changed with various commands.
rng = np.random.default_rng(1)
original_array = np.floor(10 * rng.random((3, 4)))

print(f"Original Array [Shape {original_array.shape}]:\n{original_array}")

# Return to a flattened array.
flattened_array = original_array.ravel()
print(f"Flattened Array [Shape {flattened_array.shape}]:\n{flattened_array}")

# Modify array's shape.
modified_array = original_array.reshape(2, 6)
print(f"Reshaped Array [Shape {modified_array.shape}]:\n{modified_array}")

# Transpose array.
transeposed_array = original_array.T
print(f"Transeposed Array [Shape {transeposed_array.shape}]:\n{transeposed_array}")

# "reshape" is an immutable method.
original_array.reshape(2, 6)
print(f'After "reshape": \n{original_array}')

# "resize" is a mutable method, it will modify array itself.
original_array.resize(2, 6)
print(f'After "resize": \n{original_array}')
