import numpy as np
from numpy import ndarray

rng = np.random.default_rng(1)


def display(name: str, array: ndarray):
    print(f"Array {name} [Shape {array.shape}]:\n{array}")


original_array = np.array(10 * rng.random((2, 10)), dtype=np.int8)
display("Original", original_array)

splited_array_2 = np.hsplit(original_array, 2)
print(f"Horizontal splited original_array into 2:\n{splited_array_2}")

splited_array_5 = np.hsplit(original_array, 5)
print(f"Horizontal splited original_array into 5:\n{splited_array_5}")
