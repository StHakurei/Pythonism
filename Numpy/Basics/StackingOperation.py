import numpy as np
from numpy import ndarray

rng = np.random.default_rng(1)


def display(name: str, array: ndarray):
    print(f"Array {name} [Shape {array.shape}]:\n{array}")


# Several arrays can be stacked together along different axes.
a = np.array(10 * rng.random((2, 6)), dtype=np.int8)
b = np.array(10 * rng.random((2, 6)), dtype=np.int8)
c = np.array(10 * rng.random((2, 6)), dtype=np.int8)
display("a", a)
display("b", b)
display("c", c)

vStack_array = np.vstack((a, b))
print(f"Vertical stack array a with b:\n{vStack_array}")

hStack_array = np.hstack((a, b))
print(f"Horizontal stack array a with b:\n{hStack_array}")
