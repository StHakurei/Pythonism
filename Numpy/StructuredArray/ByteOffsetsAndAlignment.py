import numpy as np

# NumPy provides two methods to automatically determine the field byte offsets
# and the overall itemsize of a structured datatype.
# For byte offsets and aligment -> https://en.wikipedia.org/wiki/Data_structure_alignment

# This is a typical aligned data structure.
# All itmes inside the structure are 2-Character String (8 bytes).
aligned_type = np.dtype("U2, U2, U2, U2, U2")
print(f"Offsets(byte): {[aligned_type.fields[name][1] for name in aligned_type.names]}")
print(f"Itemsize: {aligned_type.itemsize} bytes.")

# This is an unaligne data structure.
# "u1" is Unsigned-Integer (1 byte).
# "i4" is Singed-Integer (4 bytes).
# "i8" is Singed-Integer (8 bytes).
# "U2" is 2-Character String (8 bytes).
unalign_type = np.dtype("u1, i4, i8, U2")
print(
    f"\nOffsets(byte): {[unalign_type.fields[name][1] for name in unalign_type.names]}"
)
print(f"Itemsize: {unalign_type.itemsize} bytes.")

# Use "align=True" to let NumPy padding the structure in the same way many C-Compilers would
# pad a C-Struct.
# An n-byte aligned address would have a minimum of log2(n) least-significant zeros when expressed in binary.
auto_aligned_type = np.dtype("u1, i4, i8, U2", align=True)
print(
    f"\nOffsets(byte): {[auto_aligned_type.fields[name][1] for name in auto_aligned_type.names]}"
)
print(f"Itemsize: {auto_aligned_type.itemsize} bytes.")
