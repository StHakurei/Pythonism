import numpy as np

# Assign data from Python Native Tuples.
array_0 = np.array([(-100, 100, 0.99), (-101, 101, 0.98)], dtype="i8, u8, f8")
print(f"Before: {array_0}")
array_0[1] = (-10, 10, 1.10)
print(f"After: {array_0}")

# Assign data from Scalars.
# A scalar assigned to a structured element will be assigned to all fields.
array_1 = np.zeros(3, dtype="i8, f4, ?, U1")
print(f"Before: {array_1}")
array_1[:] = 3
print(f"After: {array_1}")
array_1[:] = np.arange(3)
print(f"After: {array_1}")

# Assignment from other Structured Arrays.
# Assignment between two structured arrays occurs as if the source
# elements had been converted to tuples and then assigned to the
# destination elements.
array_2 = np.zeros(3, dtype=[("First", "i8"), ("Second", "f4"), ("Third", "U2")])
array_3 = np.ones(3, dtype=[("First", "f4"), ("Second", "U3"), ("Third", "i8")])
print(f"Original array_2: {array_2}")
print(f"Original array_3: {array_3}")
array_3[:] = array_2
print(f"After: {array_3}")
