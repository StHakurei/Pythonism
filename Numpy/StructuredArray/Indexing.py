import numpy as np

sample_array = np.array(
    [("Peter", 10), ("Mary", 34), ("Andrew", 4)], dtype=[("Name", "U20"), ("ID", "u8")]
)
print(f"Original: {sample_array}")

# Individual fields of a structured array may be accessed
# and modified by indexing the array with the field name.
name_array = sample_array["Name"]
id_array = sample_array["ID"]
print(f"Name Array: {name_array}")
print(f"ID Array: {id_array}")

# Modify the field "ID".
sample_array["ID"] = 0
print(f"After modifying ID: {sample_array}")

# Structure comparison.
array_0 = np.zeros(3, dtype=[("a", "i4"), ("b", "i4")])
array_1 = np.ones(3, dtype=[("a", "i4"), ("b", "i4")])
print(f"Structure Comparison: {array_0 == array_1}")
