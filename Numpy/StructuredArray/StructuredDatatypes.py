# Structured arrays are ndarrays whose datatype is a composition of simpler datatypes
# organized as a sequence of named fields.
import numpy as np

# Simpliy to create a field called "age" with data type integer.
age = np.dtype(np.int32)

# And you can check this data type attributes by below methods.
print(f"itemsize: {age.itemsize}")  # in Bytes.
print(f"typename: {age.name}")
print(f"byteorder: {age.byteorder}")  # Will explain later.

# Since age will never be negative, you may want to use unsigned_int here.
# "u4" is an Array-protocol type strings.
# The first character specifies the kind of data
# and the remaining characters specify the number of bytes per item.
# For more information -> https://numpy.org/doc/stable/reference/arrays.dtypes.html#arrays-dtypes-constructing
newAge = np.dtype("u4")  # "u4" represents 4 bytes unsigned_integer.
print(f"itemsize: {newAge.itemsize}")
print(f"typename: {newAge.name}")
print(f"byteorder: {newAge.byteorder}")

ageArray = np.array([-10, 0, 10], dtype=age)
print(ageArray)
newAgeArray = np.array([0, 10, 20], dtype=newAge)
print(newAgeArray)
weirdArray = np.array([-1, 0, 1], dtype=newAge)  # The negative element will be messed.
print(weirdArray)

# Structured datatypes may be created using the function numpy.dtype.
# There are 4 alternative way to do that.
# 1. A list of tuples.
person_type_0 = np.dtype([("Name", "U20"), ("Gender", "U1"), ("Age", "u4")])
people_collection_0 = np.array(
    [("Peter", "M", 35), ("Lily", "F", 32)], dtype=person_type_0
)
print(f"Type names: {person_type_0.names}")
print(f"Type fields: {person_type_0.fields}")
print(people_collection_0)

# 2. A specified string dtype.
person_type_1 = np.dtype("U20, U1, u4")
people_collection_1 = np.array(
    [("Peter", "M", 35), ("Lily", "F", 32)], dtype=person_type_1
)
print(f"Type names: {person_type_1.names}")
print(f"Type fields: {person_type_1.fields}")
print(people_collection_1)

# 3. A dictionary of field parameter arrays
person_type_2 = np.dtype(
    {"names": ["Name", "Gender", "Age"], "formats": ["U20", "U1", "u4"]}
)
people_collection_2 = np.array(
    [("Peter", "M", 35), ("Lily", "F", 32)], dtype=person_type_2
)
print(f"Type names: {person_type_2.names}")
print(f"Type fields: {person_type_2.fields}")
print(people_collection_2)
