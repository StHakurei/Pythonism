# SIFT uses 128-dim vector for descriptors and since it using float point numbers,
# it takes basically 512 bytes. Creating such a vector for thousands of features takes
# a lof of memory which are not feasible for resource constraint applications.
# 
# BRIEF comes into picture at this moment. It provides a shortcut to find the binary 
# strings directly without finding descriptors.
#
# One important point is that BRIEF is a feature descriptor, it doesn't provide any method 
# to find the features. So you will have to use any other feature detectors like SIFT, SURF etc.
# The paper recommends to use CenSurE which is a fast detector and BRIEF works 
# even slightly better for CenSurE points than for SURF points.
from numpy import ndarray
import numpy as np
import cv2

SAMPLE_IMAGE_FILE = "./ishtar.jpeg"

# Display Numpy array attributes.
def showArrayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")

grayImage = cv2.cvtColor(cv2.imread(SAMPLE_IMAGE_FILE), cv2.COLOR_BGR2GRAY)

# Initiate STAR (CenSurE) detector.
star = cv2.xfeatures2d.StarDetector_create()

# Initiate BRIEF extractor.
brief = cv2.xfeatures2d.BriefDescriptorExtractor_create()

# Find keypoints with STAR.
keypoints = np.array(star.detect(grayImage))

# Compute descriptors with BERIF.
tupleResult = brief.compute(grayImage, keypoints)

# Show array attributes of the keypoints and the descriptors.
print("=== Output Keypoints Array information ===")
showArrayAttributes(keypoints)
print("=== Output Descriptors Array information ===")
showArrayAttributes(tupleResult[1])