# ORB is an effecient alternative to SIFT and SURF but for free.
# 
from numpy import ndarray
import numpy as np
import cv2

SAMPLE_IMAGE_FILE = "./ishtar.jpeg"

# Display Numpy array attributes.
def showArrayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")

grayImage = cv2.cvtColor(cv2.imread(SAMPLE_IMAGE_FILE), cv2.COLOR_BGR2GRAY)

# Initiate ORB algorithm.
orb = cv2.ORB_create()

# Find keypoints with ORB.
keypoints = np.array(orb.detect(grayImage))

# Compute descriptors with ORB.
tupleResult = orb.compute(grayImage, keypoints)

# Show array attributes of the keypoints and the descriptors.
print("=== Output Keypoints Array information ===")
showArrayAttributes(keypoints)
print("=== Output Descriptors Array information ===")
showArrayAttributes(tupleResult[1])