# Scale Invariant Feature Transform (SIFT).
# SIFT is good at recognizing scaled image.
# This sample code exemplifies that how to find SIFT Keypoints and Descriptors.
from numpy import ndarray
import numpy as np
import cv2

SAMPLE_IMAGE_FILE = "./saber.jpeg"

# Display Numpy array attributes.
def showArrayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")


# Read image and convert it to grayscale.
grayImage = cv2.cvtColor(cv2.imread(SAMPLE_IMAGE_FILE), cv2.COLOR_BGR2GRAY)

# Initial SIFT algorithm.
sift = cv2.SIFT_create()

# Get SIFT keypoints.
keypoints = np.array(sift.detect(grayImage))

# GET SIFT descriptors.
# The result of "compute()" will return a tuple, (keypoints, descriptors)
# The descriptors is a numpy array of shape (Number of Keypoints)×128.
tupleResult = sift.compute(grayImage, keypoints)


# Show array attributes of the keypoints and the descriptors.
print("=== Output Keypoints Array information ===")
showArrayAttributes(keypoints)
print("=== Output Descriptors Array information ===")
showArrayAttributes(tupleResult[1])

# Draw SIFT Keypoints in the image.
# cv2.drawKeypoints(input_image, keypoints, output_image)
keypointsImage = cv2.drawKeypoints(grayImage, keypoints, grayImage)
cv2.imshow("Draw SIFT Keypoints on the image. (Press any key to exit)", keypointsImage)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Draw keypoints with flag.
# Apply cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS flag to function "drawKeyPoints()"
# to show keypoint with its orientation.
keypointsWithOrientationImage = cv2.drawKeypoints(
    grayImage, keypoints, grayImage, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
)
cv2.imshow(
    "Keypoint with its orientation. (Press any key to exit)",
    keypointsWithOrientationImage,
)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Save these images into files.
cv2.imwrite("saber_keypointsImage.jpeg", keypointsImage)
cv2.imwrite("saber_keypointsWithOrientationImage.jpeg", keypointsWithOrientationImage)
