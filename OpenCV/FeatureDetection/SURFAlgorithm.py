# Speeded-Up Robust Features (SURF).
# SURF is 3 times faster than SIFT while performance is comparable with SIFT,
# and it's good at handling images with blurring and rotation, but not good
# at handling viewpoint change and illumination changes.
from numpy import ndarray
import numpy as np
import cv2

SAMPLE_IMAGE_FILE = "./ishtar.jpeg"

# Display Numpy array attributes.
def showArrayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")

# Read image and convert it to grayscale.
grayImage = cv2.cvtColor(cv2.imread(SAMPLE_IMAGE_FILE), cv2.COLOR_BGR2GRAY)

# Initial SURF algorithm.
# Hessian Threshold decides the keypoints quantity,
# higher the threshold, lesser the keypoints.
surf400 = cv2.xfeatures2d.SURF_create(400)
surf50000 = cv2.xfeatures2d.SURF_create(50000)

keypointsWith400Threshold = np.array(surf400.detect(grayImage))
keypointsWitt50000Threshold = np.array(surf50000.detect(grayImage))

print(f"Keypoints size (Hessian threshold 400): {len(keypointsWith400Threshold)}")
print(f"Keypoints size (Hessian threshold 50000): {len(keypointsWith50000Threshold)}")