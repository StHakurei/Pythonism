# OpenCV provide feature matchers to let you compare the features of an image
# with others.
#
# Brute-Force matcher is simple. It takes the descriptor of one feature 
# in first set and is matched with all other features in second set using 
# some distance calculation. And the closest one is returned.
from numpy import ndarray
import matplotlib.pyplot as plt
import numpy as np
import cv2

SABER_IMAGE_FILE = "saber.jpeg"
ISHTAR_IMAGE_FILE = "ishtar.jpeg"

# Display Numpy array attributes.
def showArrayAttributes(object: ndarray):
    print(f"The dimension of the array: {object.ndim}")
    print(f"The dimension of the array with the size of each dimension: {object.shape}")
    print(f"The total elements of the array: {object.size}")
    print(f"The data type of the array (object): {object.dtype.name}")
    print(f"The size in bytes of each element of the array: {object.itemsize}")
    print(f"The buffer containg the actual elements of the array: {object.data}")
    print(f"Object type: {type(object)}")

saberGrayImage = cv2.cvtColor(cv2.imread(SABER_IMAGE_FILE), cv2.COLOR_BGR2GRAY)
ishtarGrayImage = cv2.cvtColor(cv2.imread(ISHTAR_IMAGE_FILE), cv2.COLOR_BGR2GRAY)

# In this sample we use ORB algorithm to detect the keypoints and descriptors.
orb = cv2.ORB_create()

# Syntax = orb.detectAndCompute(INPUT_IMAGE, MASK)
# We will explain parameter "MASK" later.
saberKeypoints, saberDescriptors = orb.detectAndCompute(saberGrayImage, None)
ishtarKeypoints, ishtarDescriptors = orb.detectAndCompute(ishtarGrayImage, None)

print("=== Output Descriptors Array information ===")
showArrayAttributes(saberDescriptors)
print("=== Output Descriptors Array information ===")
showArrayAttributes(ishtarDescriptors)

# Initiate Brute-Force matcher with distance measurement "NORM_HAMMING".#
# Since we use ORB, this measurement and crossCheck is switched on for better results.
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

# Match descriptors.
matches = np.array(bf.match(saberDescriptors, ishtarDescriptors))
print("=== Output Matches Array information ===")
showArrayAttributes(matches)
print(matches[0:10])

# Sort matches in the order of their distance.
sortedMatches = np.array(sorted(matches, key = lambda x:x.distance))
showArrayAttributes(sortedMatches)
print(sortedMatches[0:10])

# The result of bf.match(des1, des2) is a list of DMatch objects and it has
# following attributes:
#
# DMatch.distance - Distance between descriptors. The lower, the better it is.
# DMatch.trainIdx - Index of the descriptor in train descriptors
# DMatch.queryIdx - Index of the descriptor in query descriptors
# DMatch.imgIdx   - Index of the train image.

# As you can see, self-match returns 500 matches,
# which equals to the size of first dimension of saber's descriptors.
selfMatches = np.array(bf.match(saberDescriptors, saberDescriptors))
print(f"Size of each dimension: {saberDescriptors.shape}")
print(f"Size of each dimension: {selfMatches.shape}")
print(f"Match rate: {selfMatches.shape[0]/saberDescriptors.shape[0]}")

# The matches of saber and ishtar images.
# Because their match rate is lower than 0.3, so you know these
# two images are not same in a high chance.
print(f"Size of each dimension: {saberDescriptors.shape}")
print(f"Size of each dimension: {sortedMatches.shape}")
print(f"Match rate: {sortedMatches.shape[0]/saberDescriptors.shape[0]}")


# Draw first 10 matches on an image.
matchesImage = cv2.drawMatches(
    saberGrayImage, 
    saberKeypoints, 
    ishtarGrayImage, 
    ishtarKeypoints, 
    sortedMatches[0:10], 
    None, 
    flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

# Show image.
plt.imshow(matchesImage),plt.show()













