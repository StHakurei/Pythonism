# Find the best matches by using knnMatch() in rotated and scaled image.
import cv2
import matplotlib.pyplot as plt

QUARY_IMAGE = "saber_alternative.jpeg"
TRAINING_IMAGE = "saber.jpeg"

queryGrayImage = cv2.cvtColor(cv2.imread(QUARY_IMAGE), cv2.COLOR_BGR2GRAY)
trainingGrayImage = cv2.cvtColor(cv2.imread(TRAINING_IMAGE), cv2.COLOR_BGR2GRAY)

# Initiate ORB detector
orb = cv2.ORB_create()

queryKeypoints, queryDescriptors = orb.detectAndCompute(queryGrayImage, None)
trainingKeypoints, trainingDescriptors = orb.detectAndCompute(trainingGrayImage, None)

# BFMatcher with default params
bf = cv2.BFMatcher()
matches = bf.knnMatch(queryDescriptors,trainingDescriptors,k=2)

goodMatches = []
for match1, match2 in matches:
    if match1.distance < 0.75 * match2.distance:
        goodMatches.append([match1])

# cv2.drawMatchesKnn expects list of lists as matches.
resultImage = cv2.drawMatchesKnn(
    queryGrayImage,
    queryKeypoints,
    trainingGrayImage,
    trainingKeypoints,
    goodMatches,
    None,
    flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

# Show image.
plt.imshow(resultImage),plt.show()
