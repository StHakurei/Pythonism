from multiprocessing import Process
import os
import platform
import time

# is_alive method determines if the process is runing.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker(worker_id):
    print(f"worker process {worker_id} - start")
    time.sleep(10)
    print(f"worker process {worker_id} - terminated")


def main():
    print(f"Starting main process.")
    process = Process(target=worker, args=("10",))
    process.start()

    while process.is_alive():
        print("Worker process is running.")
        time.sleep(1)

    process.join()
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
