from multiprocessing import Process, Queue
import os
import platform
import time
import secrets

# To pass message, we can utilize the pipe for the connection between processes.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker(worker_id, queue):
    print(f"worker process {worker_id} - start")
    print(f"pid {os.getpid()} - ppid {os.getppid()}")
    time.sleep(2)
    queue.put(f"{worker_id} - {secrets.token_hex(16)}")
    print(f"worker process {worker_id} - terminated")


def main():
    message_slot = Queue()
    print(f"Starting main process. pid: {os.getpid()}")
    processes = [
        Process(
            target=worker,
            args=(
                id,
                message_slot,
            ),
        )
        for id in range(3)
    ]

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    results = [message_slot.get() for _ in processes]
    print(f"Result from main process: {results}")
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
