from multiprocessing import Pool
import functools
import os
import platform
import time

# This sample code performs run multiple functions in a pool.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def service_a():
    print(f"service_a pid {os.getpid()}: ppid {os.getppid()}- start")
    time.sleep(3)
    print(f"service_a pid {os.getpid()}: ppid {os.getppid()}- terminated")


def service_b():
    print(f"service_b pid {os.getpid()}: ppid {os.getppid()}- start")
    time.sleep(4)
    print(f"service_b pid {os.getpid()}: ppid {os.getppid()}- terminated")


def service_c():
    print(f"service_c pid {os.getpid()}: ppid {os.getppid()}- start")
    time.sleep(5)
    print(f"service_c pid {os.getpid()}: ppid {os.getppid()}- terminated")


def service_d():
    print(f"service_d pid {os.getpid()}: ppid {os.getppid()}- start")
    time.sleep(6)
    print(f"service_d pid {os.getpid()}: ppid {os.getppid()}- terminated")


def smap(func):
    return func()


def main():
    print(f"Starting main process. pid: {os.getpid()}")
    func_a = functools.partial(service_a)
    func_b = functools.partial(service_b)
    func_c = functools.partial(service_c)
    func_d = functools.partial(service_d)

    with Pool() as pool:
        pool.map(smap, [func_a, func_b, func_c, func_d])
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
