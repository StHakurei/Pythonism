from multiprocessing import Process
import os
import platform
import time

# Join method blocks the execution of main process until the children is terminated.
# Without the join method, the main process won't wait until the process gets terminated.
# We can see the children processes don't output information in sequoence, this problem
# could be solved through Queue.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker(worker_id):
    print(f"worker process {worker_id} - start")
    print(f"pid {os.getpid()} - ppid {os.getppid()}")
    time.sleep(2)
    print(f"worker process {worker_id} - terminated")


def main():
    print(f"Starting main process. pid: {os.getpid()}")
    process_0 = Process(target=worker, args=("10",))
    process_0.start()

    process_1 = Process(target=worker, args=("20",))
    process_1.start()

    process_2 = Process(target=worker, args=("30",))
    process_2.start()

    process_3 = Process(target=worker, args=("40",))
    process_3.start()

    process_0.join()
    process_1.join()
    process_2.join()
    process_3.join()
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
