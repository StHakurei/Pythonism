from multiprocessing import Pool, cpu_count
import os
import platform
import time

# Manage sub-processes can be simplified with the Pool object.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {cpu_count()}
    """
    print(info)


def worker(worker_id, sleep_time):
    print(f"worker process {worker_id} - start")
    print(f"pid {os.getpid()} - ppid {os.getppid()}")
    time.sleep(sleep_time)
    print(f"worker process {worker_id} - terminated")


def main():
    ids = [["10", 1], ["20", 2], ["30", 3], ["40", 4]]
    print(f"Starting main process. pid: {os.getpid()}")
    with Pool() as pool:
        pool.starmap(worker, ids)
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
