from multiprocessing import Process, Queue
import os
import platform
import time
import secrets

# In multiprocessing, there's no guarantee that the process finish in a certain order.
# To maintain the order of process, we need to give extra index for each input value.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker(index, queue):
    print(f"worker process - start")
    print(f"pid {os.getpid()} - ppid {os.getppid()}")
    queue.put([index, secrets.token_hex(16)])
    print(f"worker process - terminated")


def main():
    message_slot = Queue()
    print(f"Starting main process. pid: {os.getpid()}")
    processes = [
        Process(target=worker, args=(index, message_slot))
        for index in range(os.cpu_count())
    ]

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    raw_result = [message_slot.get() for _ in processes]
    print(f"Original output: {raw_result}")
    sorted_result = [tuple[1] for tuple in sorted(raw_result)]
    print(f"Sorted output: {sorted_result}")
    print(f"Terminated main process.")


# As you can see in the terminal, processes will start and terminated disorderly,
# but result will be sorted according to the index you gave.
if __name__ == "__main__":
    show_sys_info()
    main()
