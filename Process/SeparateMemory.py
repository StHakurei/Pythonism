from multiprocessing import Process
import os
import platform
import time

# In multiprocessing, each worker process has its own memory, the memory is not shared like
# threading, so you cannot share data between processes by implementing 'global' keyword.

sample_data = [1, 2]


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker():
    global sample_data
    print(f"worker process - start")
    print(f"pid {os.getpid()} - ppid {os.getppid()}")
    sample_data.extend([3, 4, 5])
    print(f"Result in worker process: {sample_data}")
    print(f"worker process - terminated")


def main():
    print(f"Starting main process. pid: {os.getpid()}")
    process_0 = Process(target=worker)
    process_0.start()
    process_0.join()
    print(f"Result in main process: {sample_data}")
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
