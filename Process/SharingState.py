from multiprocessing import Process, Value, Array
import os
import platform
import time

# Data can be stored in a shared memory using Value or Array.
# Note: It is best to avoid sharing data between processes. Message passing is preferred.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker(data):
    print(f"worker process - start")
    print(f"pid {os.getpid()} - ppid {os.getppid()}")
    with data.get_lock():
        data.value = data.value + 100
    print(f"result: {data.value}")
    print(f"worker process - terminated")


def main():
    sample_data = Value("i", 0)
    print(f"Starting main process. pid: {os.getpid()}")
    process_0 = Process(target=worker, args=(sample_data,))
    process_0.start()

    process_1 = Process(target=worker, args=(sample_data,))
    process_1.start()

    process_2 = Process(target=worker, args=(sample_data,))
    process_2.start()

    process_3 = Process(target=worker, args=(sample_data,))
    process_3.start()

    process_0.join()
    process_1.join()
    process_2.join()
    process_3.join()
    print(f"result in main process: {sample_data.value}")
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
