from multiprocessing import Process
import os, platform

# Both processes and threads are independent sequoences of exection, but processes run
# in separated memory (process isolation) and threads run in shared memory. Compare with
# threads, create and destroy processes are slower and thier children can become zombies
# if you do things wrong, besides, processes use more memory and more overhead.
#
# Multiprocessing is recommended when the tasks are CPU intensive and you have a powerful
# multi-core CPU.
#
# About Python GIL.
# A global interpreter lock (GIL) is a mechanism used in Python interpreter to synchronize
# the execution of threads so that only one native thread can execute at a time,
# even if run on a multi-core processor.
#
# The C extensions, such as numpy, can manually release the GIL to speed up computations.
# Also, the GIL released before potentionally blocking I/O operations.


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def worker(worker_id):
    print(f"worker process {worker_id}")


def main():
    process = Process(target=worker, args=("10",))
    process.start()


if __name__ == "__main__":
    show_sys_info()
    main()
