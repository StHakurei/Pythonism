from multiprocessing import Process
import os
import platform
import time

# We can override the run method through subclassing process.


class Service(Process):
    def __init__(self, worker_id):
        super(Process, self).__init__()
        self.worker_id = worker_id

    def run(self):
        print(f"worker process {self.worker_id} - start")
        print(f"pid {os.getpid()} - ppid {os.getppid()}")
        time.sleep(3)
        print(f"worker process {self.worker_id} - terminated")


def show_sys_info():
    info = f"""
    System: {platform.system()} {platform.version()}
    Processor: {platform.processor()}
    Available CPU: {os.cpu_count()}
    """
    print(info)


def main():
    print(f"Starting main process. pid: {os.getpid()}")
    service_0 = Service("10")
    service_0.start()

    service_1 = Service("20")
    service_1.start()
    print(f"Terminated main process.")


if __name__ == "__main__":
    show_sys_info()
    main()
