import threading
import time
import datetime


# Basic thread sample code with starting many threads.
def get_timestamp():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def thread_function(name):
    print(f"{get_timestamp()} - Starting thread {name}")
    time.sleep(3)
    print(f"{get_timestamp()} - Finishing thread {name}")


# Create 3 sub-threads and wait them to be finished.
if __name__ == "__main__":
    threads = list()
    for id in range(3):
        print(f"{get_timestamp()} - create thread {id}.")
        thread = threading.Thread(target=thread_function, args=(id,))
        threads.append(thread)
        thread.start()

    for id, thread in enumerate(threads):
        print(f"{get_timestamp()} - Waiting sub-thread {id}")
        thread.join()
        print(f"{get_timestamp()} - thread {id} finished")

    print(f"{get_timestamp()} - Main thread finished.")
