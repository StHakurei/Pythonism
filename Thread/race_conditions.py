import threading
import time
import datetime
import concurrent.futures


# WARNING: This sample code is a bad example. DO NOT copy it into your code.
# Race Conditions can occur when two or more threads access a shared piece of data.
# This sample code will show you the effect.

# Create a message box to store message.
class MessageBox(object):
    def __init__(self):
        self.msg = None
        self.count = 0

    def get_timestamp(self):
        return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    def update(self, msg, thread_Id):
        local_copy = msg
        self.msg = local_copy
        print(
            f"{self.get_timestamp()} - Message has been changed by thread {thread_Id}."
        )
        print(f"Message: {self.msg}")


def get_timestamp():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


# Create 10 sub-thread to change the content of MessageBox.
# See difference with synchronization.py
if __name__ == "__main__":
    message_list = ["I'm Groot!", "I'm Batman!"]
    box = MessageBox()
    print(f"{get_timestamp()} - Starting main thread.")
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for id in range(10):
            executor.submit(box.update, message_list[0], id)
            executor.submit(box.update, message_list[1], id + 10)
    print(f"Final message: {box.msg}")
    print(f"The message has been cahnged {box.count} times.")
    print(f"{get_timestamp()} - Main thread finished.")
