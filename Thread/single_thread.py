import threading
import time
import datetime


# Basic thread sample code with starting a single thread.
def get_timestamp():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def thread_function(name):
    print(f"{get_timestamp()} - Starting thread {name}")
    time.sleep(2)
    print(f"{get_timestamp()} - Finishing thread {name}")


# Running thread without "daemon", main thread will end immediately regardless
# sub-thread is in running or already finished, but sub-thread will not be terminated.
if __name__ == "__main__":
    print(f"{get_timestamp()} - Main thread started.")
    thread = threading.Thread(target=thread_function, args=(1,))
    thread.start()
    print(f"{get_timestamp()} - Main thread finished.")
