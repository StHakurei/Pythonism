import threading
import time
import datetime
import concurrent.futures


# Basic synchronization by using Lock().
# Create a message box to store message.
class MessageBox(object):
    def __init__(self):
        self.msg = None
        self.count = 0
        self._lock = threading.Lock()

    def get_timestamp(self):
        return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    def update(self, msg, thread_Id):
        with self._lock:
            self.count = self.count + 1
            local_copy = msg
            self.msg = local_copy
            print(
                f"{self.get_timestamp()} - Message has been changed by thread {thread_Id}."
            )
            print(f"Message: {self.msg}")


def get_timestamp():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


# Create 10 sub-thread to change the content of MessageBox.
# See difference with reac_conditions.py
if __name__ == "__main__":
    message_list = ["I'm Groot!", "I'm Batman!"]
    box = MessageBox()
    print(f"{get_timestamp()} - Starting main thread.")
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for id in range(10):
            executor.submit(box.update, message_list[0], id)
            executor.submit(box.update, message_list[1], id + 10)
    print(f"Final message: {box.msg}")
    print(f"The message has been cahnged {box.count} times.")
    print(f"{get_timestamp()} - Main thread finished.")
