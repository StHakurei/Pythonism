import threading
import time
import datetime
import concurrent.futures


# Basic thread sample code with starting many threads.
def get_timestamp():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def thread_function(name):
    print(f"{get_timestamp()} - Starting thread {name}")
    time.sleep(3)
    print(f"{get_timestamp()} - Finishing thread {name}")


# Create 10 sub-threads with ThreadPoolExecutor.
# The end of the with block causes the ThreadPoolExecutor to do a .join()
# on each of the threads in the pool.
if __name__ == "__main__":
    print(f"{get_timestamp()} - Starting main thread.")
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        executor.map(thread_function, range(10))
    print(f"{get_timestamp()} - Main thread finished.")
